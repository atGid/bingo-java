import java.io.*;
import java.util.*;
import java.lang.*;

public class BingoGame {
	public static void main (String [] args) {
	
	int [][] card = new int [5][5]; //Array that holds our bingo card
	BitSet match = new BitSet(76);// Bitset to store numbers chosen
	fillCard(card);	//Fill our array with data from bingo.in
	
	printCard(card);//Print card

	playGame(card, match);// Play the game
	
	System.out.println();
	}//End of main
	

	//Methods
	public static void playGame(int [][] card, BitSet match)
	{
		System.out.println("\t"+ "BINGO NUMBERS PICKED AT RANDOM FROM BIN :");
		int win = 0;
		int counts =0;
		while(win == 0)
		{
			
			counts += printRandom(match, card, counts);
			win = checkForWin(card);	
		}
		System.out.println();
		String bingoType = "";
		if(win == 1)
			bingoType = "HORIZONTAL";
		if(win == 2)
			bingoType = "VERTICAL";
		if(win == 3)
			bingoType = "DIAGONAL";
		System.out.println("YOU WIN A " + bingoType + " BINGO AFTER " + counts + " PICKS!");
		System.out.println();
		System.out.println("YOUR BINGO CARD: ");
		System.out.println();
		printCard(card);
	}
	

	public static int checkForWin(int [][] array)
	{
		int hsum0=0, hsum1=0, hsum2=0, hsum3=0, hsum4=0;
		int vsum0=0, vsum1=0, vsum2=0, vsum3=0, vsum4=0;
		int dsum1=0, dsum2=0;
		int sum = 0;

		for(int i = 0; i < array.length; i++)
           	{
                        for(int j = 0; j < array[0].length; j++)
                                hsum0 += array[0][j];
			for(int j = 0; j < array[1].length; j++)
				hsum1 += array[1][j];
			for(int j = 0; j < array[2].length; j++)
				hsum2 += array[2][j];
			for(int j = 0; j < array[3].length; j++)
				hsum3 += array[3][j];
			for(int j = 0; j < array[4].length; j++)
				hsum4 += array[4][j]; 	        
		}

		for(int j = 0; j < array[0].length; j++)
		{
			for(int i = 0; i < array.length; i++)
				vsum0 += array[i][0];
			for(int i = 0; i < array.length; i++)
				vsum1 += array[i][1];
			for(int i = 0; i < array.length; i++)
				vsum2 += array[i][2];
			for(int i = 0; i < array.length; i++)
				vsum3 += array[i][3];
			for(int i = 0; i < array.length; i++)
				vsum4 += array[i][4];
		}	

		for(int i = 0; i < array.length; i++)
		{
			dsum1+= array[i][i];
		}
		for(int i =0; i < array.length; i++)
		{
			dsum2+= array[i][array.length-1-i];
		}
		if(hsum0 == 0 || hsum1 == 0 || hsum2 == 0 || hsum3 == 0 || hsum4 == 0)
			sum = 1;
		if(vsum0 == 0 || vsum1 == 0 || vsum2 == 0 || vsum3 == 0 || vsum4 == 0) 
			sum = 2;
		if(dsum1 == 0 || dsum2 ==0)
			sum = 3;
		return sum;
	}

	public static void checkMatch(int [][] array, int num)
	{
		boolean match = false;
		for(int i=0; i < array.length; i++)
		{
			for(int j=0; j < array[0].length; j++)
			{
				if(num== array[i][j])
				{
					markCard(i, j, array);
				}
			}
		}
	}

	public static int printRandom(BitSet match, int [][] array, int count)
	{
		int rNum;
		int increment = 0;
		rNum = (int)(Math.random() * 74.0 + 1.0);
		if(!match.get(rNum))
		{
			match.set(rNum);
			System.out.print(rNum + "    ");
			if(rNum < 10)
				System.out.print(" ");
			checkMatch(array, rNum);
			increment = 1;
		}
		if(increment == 1 && (count == 14 || count == 29 || count == 44 || count == 59))
		{
			System.out.println();
		}
		return increment;
	}
	public static void markCard(int i, int j, int [][] array)
	{
		array[i][j] = 0;
	}	
	public static void printCard(int [][] card)
	{
		System.out.println();
		System.out.println("\t" + "YOUR BINGO CARD: ");
		System.out.println( "\t" + "  B" + "    " + "I" + "    " + "N" + "    " + "G" + "    " + "O");
		System.out.println("\t" + "-------------------------");
		for(int i=0; i<5; i++)
		{
			System.out.print("\t");
			for(int j=0; j<5; j++)
			{
				System.out.print( "| ");
				if(card[i][j] < 10)
                                        System.out.print(" ");
				if(card[i][j] == 0)
					System.out.print("X ");
				else
					System.out.print( card[i][j] + " ");
			}
			System.out.println();
			System.out.println("\t" + "-------------------------");

		}
	}
	public static void fillCard(int [][] card)
	{
		//Take bingo.in and fill our card
		try
		{
			String filename = "bingo.in";
			Scanner scan = new Scanner(System.in);
			scan = new Scanner(new File(filename));

			while(scan.hasNext())
			{
				int num = 0;
				for(int i=0; i < 5; i++)
				{
					for(int j=0; j < 5; j++)
					{	num = scan.nextInt();
						card[i][j] = num;
					}
				}
			}
			scan.close();
		}
		catch(IOException e) {
			System.out.println(e.getMessage());}
	}
}//End of Program
